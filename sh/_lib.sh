
function exitOnError
{
	STATUS=$1
	MESSAGE="$2"

	echo "$MESSAGE" >&2
	exit $STATUS
}

function assertFileExist
{
	FILE="$1"
	STATUS=$2
	MESSAGE="$3"

	if [ ! -f "$FILE" ]
	then
		exitOnError $STATUS "$MESSAGE"
	fi
}

function assertDstFileExist
{
	FILE="$1"
	assertFileExist "$FILE" 9 "Dst file does not exist."
}

function watchAndBuild
{
	while inotifywait -q -r -e 'modify,move,create,delete' "$SRC_DIR"
	do
		echo
		./build.sh $@
		echo
	done
}

function showTime
{
	echo "[`date`]"
}

function cleanBuildingDirs
{
	echo 'Cleaning building directories.'

	rm -r "$ASM_DIR" > /dev/null 2>&1
	mkdir -p "$ASM_DIR" > /dev/null

	rm -r "$OBJ_DIR" > /dev/null 2>&1
	mkdir -p "$OBJ_DIR" > /dev/null

	rm -r "$DST_DIR" > /dev/null 2>&1
	mkdir -p "$DST_DIR/$TARGET" > /dev/null
}

function showBuildingHeader
{
	TARGET="$1"
	ENV="$2"

	echo "Building for target $TARGET and environment $ENV."
}

function showBuildingFooter
{
	DST_MAIN_FILE="$1"

	DST_MAIN_FILE_SIZE=`du -b -h "$DST_MAIN_FILE" | awk '{print $1}'`
	echo "SUCCESS ($DST_MAIN_FILE_SIZE)"
}

function showHeader
{
	HEADER="$1"

	echo "$HEADER"
}

function addDirToSrcFiles
{
	DIR="$1"

	if [ -d "$DIR" ]
	then
		SRC_FILES+=(`find "$DIR" -name *.c`)
	fi
}