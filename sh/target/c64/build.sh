#! /bin/bash

source "../../_config.sh"
source "../../_lib.sh"

showTime



TARGET=c64
TARGET_DIR="$SH_DIR/target/$TARGET"

SRC_MAIN_FILE="$SRC_DIR/$PROJECT_NAME/target/$TARGET/main.c"
DST_MAIN_FILE="$DST_DIR/$TARGET/$PROJECT_NAME"

if [ $# -lt 1 ]
then
	ENV=dev
else
	ENV="$2"
fi

ENV_FILE="$TARGET_DIR/env/_$ENV.sh"

if [ ! -f "$ENV_FILE" ]
then
	exitOnError 1 "Environment file $ENV_FILE does not exist."
fi

source "$ENV_FILE"

cleanBuildingDirs

showBuildingHeader "$TARGET" "$ENV"




showHeader 'Compiling'

OBJ_FILES=()

for SRC_FILE in "${SRC_FILES[@]}"
do
	ASM_FILE="$ASM_DIR`echo $SRC_FILE | cut -c $(( ${#SRC_DIR} + 1 ))-$(( ${#SRC_FILE} - 1 ))`o"
	mkdir -p "`dirname "$ASM_FILE"`" > /dev/null

	echo "  $ASM_FILE"
	cc65 -t c64 $COMPILING_PARAMS $MACRO_PARAMS -I "$SRC_DIR" "$SRC_FILE" -o "$ASM_FILE"

	if [ $? -gt 0 ]
	then
		exitOnError 20 'Compiling error.'
	fi

	ASM_FILES+=("$ASM_FILE")
done



showHeader 'Assembling'

OBJ_FILES=()

for ASM_FILE in "${ASM_FILES[@]}"
do
	OBJ_FILE="$OBJ_DIR`echo $ASM_FILE | cut -c $(( ${#ASM_DIR} + 1 ))-$(( ${#ASM_FILE} - 1 ))`o"
	mkdir -p "`dirname "$OBJ_FILE"`" > /dev/null

	echo "  $OBJ_FILE"
	ca65 -t c64 -o "$OBJ_FILE" "$ASM_FILE"

	if [ $? -gt 0 ]
	then
		exitOnError 21 'Assembling error.'
	fi

	OBJ_FILES+=("$OBJ_FILE")
done



showHeader 'Linking'

ld65 -t c64 $LINKING_PARAMS $MACRO_PARAMS -o "$DST_MAIN_FILE" "${OBJ_FILES[@]}" c64.lib $DEPENDENCIES_LIBS

if [ $? -gt 0 ]
then
	exitOnError 22 'Linking error.'
fi



showBuildingFooter "$DST_MAIN_FILE"