#! /bin/bash

source "../../_config.sh"
source "../../_lib.sh"

TARGET=c64
DST_MAIN_FILE="$DST_DIR/$TARGET/$PROJECT_NAME"

assertDstFileExist "$DST_MAIN_FILE"

/usr/bin/flatpak run --branch=stable --arch=x86_64 --command=x64sc net.sf.VICE "$DST_MAIN_FILE" $@

echo "Exit code: $?"
