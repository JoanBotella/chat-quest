#! /bin/bash

source "../../_config.sh"
source "../../_lib.sh"

showTime



TARGET=bash
TARGET_DIR="$SH_DIR/target/$TARGET"

SRC_MAIN_FILE="$SRC_DIR/$PROJECT_NAME/target/$TARGET/main.c"
DST_MAIN_FILE="$DST_DIR/$TARGET/$PROJECT_NAME"

if [ $# -lt 1 ]
then
	ENV=dev
else
	ENV="$2"
fi

ENV_FILE="$TARGET_DIR/env/_$ENV.sh"

if [ ! -f "$ENV_FILE" ]
then
	exitOnError 1 "Environment file $ENV_FILE does not exist."
fi

source "$ENV_FILE"

cleanBuildingDirs

showBuildingHeader "$TARGET" "$ENV"



if [ "$DEPENDENCIES" != '' ]
then
	DEPENDENCIES_CFLAGS=`pkg-config --cflags $DEPENDENCIES`
	DEPENDENCIES_LIBS=`pkg-config --libs $DEPENDENCIES`
else
	DEPENDENCIES_CFLAGS=''
	DEPENDENCIES_LIBS=''
fi



showHeader 'Compiling'

OBJ_FILES=()

for SRC_FILE in "${SRC_FILES[@]}"
do
	if [ "$SRC_FILE" != "$SRC_MAIN_FILE" ]
	then
		OBJ_FILE="$OBJ_DIR`echo $SRC_FILE | cut -c $(( ${#SRC_DIR} + 1 ))-$(( ${#SRC_FILE} - 1 ))`o"
		mkdir -p "`dirname "$OBJ_FILE"`" > /dev/null

		echo "  $OBJ_FILE"
		gcc $COMPILING_PARAMS $MACRO_PARAMS $DEPENDENCIES_CFLAGS -iquote "$SRC_DIR" -o "$OBJ_FILE" -c "$SRC_FILE" $DEPENDENCIES_LIBS

		if [ $? -gt 0 ]
		then
			exitOnError 10 'Compiling error.'
		fi

		OBJ_FILES+=("$OBJ_FILE")
	fi
done



showHeader 'Linking'

gcc $LINKING_PARAMS $MACRO_PARAMS $DEPENDENCIES_CFLAGS -iquote "$SRC_DIR" -o "$DST_MAIN_FILE" "$SRC_MAIN_FILE" "${OBJ_FILES[@]}" $DEPENDENCIES_LIBS

if [ $? -gt 0 ]
then
	exitOnError 11 'Linking error.'
fi



showBuildingFooter "$DST_MAIN_FILE"