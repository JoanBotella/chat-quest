addDirToSrcFiles "$SRC_DIR/chatQuest/target/bash"
addDirToSrcFiles "$SRC_DIR/chatQuestDemo/target/bash"

COMPILING_PARAMS="$COMPILING_PARAMS -O3"
MACRO_PARAMS="$MACRO_PARAMS -D CQ_TARGET_BASH=1 -D CQ_ENV_PROD=1"