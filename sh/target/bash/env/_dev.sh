addDirToSrcFiles "$SRC_DIR/chatQuest/target/bash"
addDirToSrcFiles "$SRC_DIR/chatQuestDemo/target/bash"
addDirToSrcFiles "$SRC_DIR/chatQuestTest"

COMPILING_PARAMS="$COMPILING_PARAMS -g"
MACRO_PARAMS="$MACRO_PARAMS -D CQ_TARGET_BASH=1 -D CQ_ENV_DEV=1"