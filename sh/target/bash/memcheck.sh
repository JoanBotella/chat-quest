#! /bin/bash

source "../../_config.sh"
source "../../_lib.sh"

TARGET=bash
DST_MAIN_FILE="$DST_DIR/$TARGET/$PROJECT_NAME"

assertDstFileExist "$DST_MAIN_FILE"

valgrind --leak-check=yes "$DST_MAIN_FILE" $@
