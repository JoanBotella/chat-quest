#ifndef CQTEST_TESTER_HEADER
#define CQTEST_TESTER_HEADER

#include "chatQuest/core/global/global.h"

void cqTest_Tester_runTest(const cq_char *name, cq_bool (*test)(void));

cq_int cqTest_Tester_test(void);

#endif