#include "chatQuestTest/tester/Tester.h"
#include "chatQuest/core/global/global.h"
#include "chatQuestTest/test/core/list/ListTest.h"

static cq_uint passCount = 0;
static cq_uint failCount = 0;

void cqTest_Tester_runTest(const cq_char *name, cq_bool (*test)(void))
{
	cq_bool passes = test();
	cq_char *icon;

	if (passes)
	{
		passCount++;
		icon = "\e[0;32m✓\e[0m";
	}
	else
	{
		failCount++;
		icon = "\e[41m✗\e[0m";
	}

	cq_formattedPrint("[%s] %s\n", icon, name);
}

static void cqTest_Tester_printResult(void)
{
	cq_print("\nResult: ");

	if (failCount > 0)
	{
		cq_formattedPrint("\e[41m%d/%d tests fail!\e[0m", failCount, passCount + failCount);
	}
	else
	{
		cq_formattedPrint("\e[0;32m%d/%d tests pass.\e[0m", passCount, passCount + failCount);
	}

	cq_print("\n");
}

cq_int cqTest_Tester_test(void)
{
	cqTest_test_core_List_test();

	cqTest_Tester_printResult();

	return failCount;
}