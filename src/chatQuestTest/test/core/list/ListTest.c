#include "chatQuestTest/test/core/list/ListTest.h"
#include "chatQuestTest/tester/Tester.h"
#include "chatQuest/core/list/List.h"
#include "chatQuest/core/global/global.h"

#define CQTEST_TEST_CORE_LISTTEST_DESTROYED_ITEMS_SIZE 4

static cq_suint destroyedItemsLogIndex = 0;
static cq_suint *destroyedItemsLog[CQTEST_TEST_CORE_LISTTEST_DESTROYED_ITEMS_SIZE];

static void resetDestroyedItemsLog(void)
{
	destroyedItemsLogIndex = 0;
	for (cq_suint i = 0; i < CQTEST_TEST_CORE_LISTTEST_DESTROYED_ITEMS_SIZE; i++)
	{
		destroyedItemsLog[i] = NULL;
	}
}

static void destroyItemWithLog(void *item)
{
	destroyedItemsLog[destroyedItemsLogIndex] = (cq_suint *) item;
	destroyedItemsLogIndex = (destroyedItemsLogIndex + 1) % CQTEST_TEST_CORE_LISTTEST_DESTROYED_ITEMS_SIZE;
	cq_free(item);
}

static void destroyItemWithoutLog(void *item)
{
	cq_free(item);
}

static cq_bool checkListItems(cq_core_List *list, cq_suint size, cq_suint *items[])
{
	cq_bool passes = CQ_TRUE;

	for (cq_suint i = 0; i < size; i++)
	{
		if (* (cq_suint *) cq_core_List_getItemAt(list, i) != *items[i])
		{
			passes = CQ_FALSE;
			break;
		}
	}

	return passes;
}

static cq_suint **buildItems(void)
{
	cq_suint **items = cq_malloc(sizeof(cq_suint *) * 4);

	items[0] = cq_malloc(sizeof(cq_suint)); *items[0] = 10;
	items[1] = cq_malloc(sizeof(cq_suint)); *items[1] = 20;
	items[2] = cq_malloc(sizeof(cq_suint)); *items[2] = 30;
	items[3] = cq_malloc(sizeof(cq_suint)); *items[3] = 40;

	return items;
}

// static cq_bool test_newListWithInvalidChunkExits()
// {
// 	// I don't know how to test this... yet
// 	return CQ_TRUE;
// }

static cq_bool test_newListHasSpecifiedReservedSizeAndChunkSize()
{
	cq_suint reservedSize = 8;
	cq_suint chunkSize = 10;
	cq_core_List *list = cq_core_List_newByReservedSizeAndChunkSize(reservedSize, chunkSize);

	cq_bool passes =
		cq_core_List_getReservedSize(list) == reservedSize
		|| cq_core_List_getChunkSize(list) == chunkSize
	;

	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);

	return passes;
}

static cq_bool test_newDefaultListHasDefaultReservedSizeAndChunkSize()
{
	cq_core_List *list = cq_core_List_new();

	cq_bool passes =
		cq_core_List_getReservedSize(list) == CQ_CORE_LIST_DEFAULT_RESERVEDSIZE
		|| cq_core_List_getChunkSize(list) == CQ_CORE_LIST_DEFAULT_CHUNKSIZE
	;

	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);

	return passes;
}

static cq_bool test_newListHasSizeZero()
{
	cq_core_List *list = cq_core_List_new();

	cq_bool passes = cq_core_List_getSize(list) == 0;

	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);

	return passes;
}

// static cq_bool test_insertItemAtInvalidIndexExits()
// {
// 	// I don't know how to test this... yet
// 	return CQ_TRUE;
// }

static cq_bool test_insertItemIncreasesSize()
{
	cq_core_List *list = cq_core_List_newByReservedSizeAndChunkSize(0, 3);

	cq_suint **items = buildItems();

	cq_bool passes;

	cq_core_List_insertItemAt(list, items[0], 0);
	passes = cq_core_List_getSize(list) == 1;

	if (passes)
	{
		cq_core_List_insertItemAt(list, items[1], 0);
		passes = cq_core_List_getSize(list) == 2;
	}

	if (passes)
	{
		cq_core_List_insertItemAt(list, items[2], 0);
		passes = cq_core_List_getSize(list) == 3;
	}

	if (passes)
	{
		cq_core_List_insertItemAt(list, items[3], 0);
		passes = cq_core_List_getSize(list) == 4;
	}

	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);
	cq_free(items);

	return passes;
}

static cq_bool test_destroyListAndItemsDestroysTheItems()
{
	cq_core_List *list = cq_core_List_new();

	cq_suint **items = buildItems();

	cq_core_List_insertItemAt(list, items[0], 0);
	cq_core_List_insertItemAt(list, items[1], 1);
	cq_core_List_insertItemAt(list, items[2], 2);
	cq_core_List_insertItemAt(list, items[3], 3);

	resetDestroyedItemsLog();

	cq_core_List_destroyListAndItems(list, &destroyItemWithLog);

	cq_bool passes =
		destroyedItemsLog[0] == items[0]
		&& destroyedItemsLog[1] == items[1]
		&& destroyedItemsLog[2] == items[2]
		&& destroyedItemsLog[3] == items[3]
	;

	cq_free(items);

	return passes;
}

// static cq_bool test_getItemAtInvalidIndexExits()
// {
// 	// I don't know how to test this... yet
// 	return CQ_TRUE;
// }

static cq_bool test_getItemReturnsAPreviouslyInsertedItem()
{
	cq_core_List *list = cq_core_List_new();

	cq_suint **items = buildItems();

	cq_core_List_insertItemAt(list, items[0], 0);
	cq_core_List_insertItemAt(list, items[1], 1);
	cq_core_List_insertItemAt(list, items[2], 2);
	cq_core_List_insertItemAt(list, items[3], 3);

	cq_bool passes = checkListItems(
		list,
		4,
		(cq_suint *[]) {items[0], items[1], items[2], items[3]}
	);

	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);
	cq_free(items);

	return passes;
}

static cq_bool test_getFirstItemReturnsTheItemAtIndexZero()
{
	cq_core_List *list = cq_core_List_new();

	cq_suint **items = buildItems();

	cq_core_List_insertItemAt(list, items[0], 0);
	cq_core_List_insertItemAt(list, items[1], 1);
	cq_core_List_insertItemAt(list, items[2], 2);
	cq_core_List_insertItemAt(list, items[3], 3);

	cq_bool passes = cq_core_List_getFirstItem(list) == items[0];

	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);
	cq_free(items);

	return passes;
}

static cq_bool test_getLastItemReturnsTheItemAtIndexSizeMinusOne()
{
	cq_core_List *list = cq_core_List_new();

	cq_suint **items = buildItems();

	cq_core_List_insertItemAt(list, items[0], 0);
	cq_core_List_insertItemAt(list, items[1], 1);
	cq_core_List_insertItemAt(list, items[2], 2);
	cq_core_List_insertItemAt(list, items[3], 3);

	cq_bool passes = cq_core_List_getLastItem(list) == items[3];

	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);
	cq_free(items);

	return passes;
}

// static cq_bool test_setChunkAnInvalidChunkExits()
// {
// 	// I don't know how to test this... yet
// 	return CQ_TRUE;
// }

static cq_bool test_insertingMoreItemsThanTheReservedSizeMakesTheStorageGrowByChunkSize()
{
	cq_uint reservedSize = 1;
	cq_uint chunkSize = 2;
	cq_core_List *list = cq_core_List_newByReservedSizeAndChunkSize(reservedSize, chunkSize);

	cq_suint **items = buildItems();

	cq_bool passes;

	cq_core_List_insertItemAt(list, items[0], 0);
	passes = cq_core_List_getReservedSize(list) == 1;

	if (passes)
	{
		cq_core_List_insertItemAt(list, items[1], 0);
		passes = cq_core_List_getReservedSize(list) == 3;
	}

	if (passes)
	{
		cq_core_List_insertItemAt(list, items[2], 0);
		passes = cq_core_List_getReservedSize(list) == 3;
	}

	if (passes)
	{
		cq_core_List_setChunkSize(list, 5);
		cq_core_List_insertItemAt(list, items[3], 0);
		passes = cq_core_List_getReservedSize(list) == 8;
	}

	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);
	cq_free(items);

	return passes;
}

static cq_bool test_insertItemsAtSomeIndexPushesBackPreviousItemsFromThatIndex()
{
	cq_core_List *list = cq_core_List_new();

	cq_suint **items = buildItems();

	cq_core_List_insertItemAt(list, items[0], 0);
	cq_core_List_insertItemAt(list, items[1], 1);
	cq_core_List_insertItemAt(list, items[2], 1);
	cq_core_List_insertItemAt(list, items[3], 2);

	cq_bool passes = checkListItems(
		list,
		4,
		(cq_suint *[]) {items[0], items[2], items[3], items[1]}
	);

	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);
	cq_free(items);

	return passes;
}

static cq_bool test_insertAsFirstItemIsTheSameOfInsertItemAtZero()
{
	cq_core_List *list = cq_core_List_new();

	cq_suint **items = buildItems();

	cq_core_List_insertAsFirstItem(list, items[0]);
	cq_bool passes = checkListItems(
		list,
		1,
		(cq_suint *[]) {items[0]}
	);

	cq_core_List_insertAsFirstItem(list, items[1]);
	passes = checkListItems(
		list,
		2,
		(cq_suint *[]) {items[1], items[0]}
	);

	cq_core_List_insertAsFirstItem(list, items[2]);
	passes = checkListItems(
		list,
		3,
		(cq_suint *[]) {items[2], items[1], items[0]}
	);

	cq_core_List_insertAsFirstItem(list, items[3]);
	passes = checkListItems(
		list,
		4,
		(cq_suint *[]) {items[3], items[2], items[1], items[0]}
	);

	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);
	cq_free(items);

	return passes;
}

static cq_bool test_insertAsLastItemIsTheSameOfInsertItemAtSize()
{
	cq_core_List *list = cq_core_List_new();

	cq_suint **items = buildItems();

	cq_core_List_insertAsLastItem(list, items[0]);
	cq_bool passes = checkListItems(
		list,
		1,
		(cq_suint *[]) {items[0]}
	);

	cq_core_List_insertAsLastItem(list, items[1]);
	passes = checkListItems(
		list,
		2,
		(cq_suint *[]) {items[0], items[1]}
	);

	cq_core_List_insertAsLastItem(list, items[2]);
	passes = checkListItems(
		list,
		3,
		(cq_suint *[]) {items[0], items[1], items[2]}
	);

	cq_core_List_insertAsLastItem(list, items[3]);
	passes = checkListItems(
		list,
		4,
		(cq_suint *[]) {items[0], items[1], items[2], items[3]}
	);

	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);
	cq_free(items);

	return passes;
}

// static cq_bool test_removeItemAtInvalidIndexExits()
// {
// 	// I don't know how to test this... yet
// 	return CQ_TRUE;
// }

static cq_bool test_removeItemDecreasesSize()
{
	cq_core_List *list = cq_core_List_new();

	cq_suint **items = buildItems();

	cq_core_List_insertAsLastItem(list, items[0]);
	cq_core_List_insertAsLastItem(list, items[1]);
	cq_core_List_insertAsLastItem(list, items[2]);
	cq_core_List_insertAsLastItem(list, items[3]);

	cq_core_List_removeItemAt(list, 1);
	cq_bool passes = cq_core_List_getSize(list) == 3;
	cq_free(items[1]);

	if (passes)
	{
		cq_core_List_removeItemAt(list, 2);
		passes = cq_core_List_getSize(list) == 2;
		cq_free(items[3]);
	}

	if (passes)
	{
		cq_core_List_removeItemAt(list, 1);
		passes = cq_core_List_getSize(list) == 1;
		cq_free(items[2]);
	}

	if (passes)
	{
		cq_core_List_removeItemAt(list, 0);
		passes = cq_core_List_getSize(list) == 0;
		cq_free(items[0]);
	}

	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);
	cq_free(items);

	return passes;
}

static cq_bool test_removeItemMovesItemsForwardFromIndex()
{
	cq_core_List *list = cq_core_List_new();

	cq_suint **items = buildItems();

	cq_core_List_insertAsLastItem(list, items[0]);
	cq_core_List_insertAsLastItem(list, items[1]);
	cq_core_List_insertAsLastItem(list, items[2]);
	cq_core_List_insertAsLastItem(list, items[3]);

	cq_core_List_removeItemAt(list, 1);
	cq_bool passes = checkListItems(
		list,
		3,
		(cq_suint *[]) {items[0], items[2], items[3]}
	);
	cq_free(items[1]);

	if (passes)
	{
		cq_core_List_removeItemAt(list, 0);
		passes = checkListItems(
			list,
			2,
			(cq_suint *[]) {items[2], items[3]}
		);
		cq_free(items[0]);
	}

	if (passes)
	{
		cq_core_List_removeItemAt(list, 1);
		passes = checkListItems(
			list,
			1,
			(cq_suint *[]) {items[2]}
		);
		cq_free(items[3]);
	}


	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);
	cq_free(items);

	return passes;
}

static cq_bool test_removeItemShrinksStorage()
{
	cq_uint reservedSize = 3;
	cq_uint chunkSize = 2;
	cq_core_List *list = cq_core_List_newByReservedSizeAndChunkSize(reservedSize, chunkSize);

	cq_suint **items = buildItems();

	cq_core_List_insertAsLastItem(list, items[0]);
	cq_core_List_insertAsLastItem(list, items[1]);
	cq_core_List_insertAsLastItem(list, items[2]);
	cq_core_List_insertAsLastItem(list, items[3]);

	cq_core_List_removeItemAt(list, 1);
	cq_free(items[1]);

	cq_bool passes = cq_core_List_getReservedSize(list) == reservedSize;

	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);
	cq_free(items);

	return passes;
}

static cq_bool test_removeItemCannotShrinkStorageToZero()
{
	cq_uint reservedSize = 3;
	cq_uint chunkSize = 3;
	cq_core_List *list = cq_core_List_newByReservedSizeAndChunkSize(reservedSize, chunkSize);

	cq_suint **items = buildItems();

	cq_core_List_insertAsLastItem(list, items[0]);
	cq_core_List_insertAsLastItem(list, items[1]);
	cq_core_List_insertAsLastItem(list, items[2]);
	cq_core_List_insertAsLastItem(list, items[3]);

	cq_core_List_removeItemAt(list, 0);
	cq_free(items[0]);
	cq_core_List_removeItemAt(list, 0);
	cq_free(items[1]);
	cq_core_List_removeItemAt(list, 0);
	cq_free(items[2]);
	cq_core_List_removeItemAt(list, 0);
	cq_free(items[3]);

	cq_bool passes = cq_core_List_getReservedSize(list) > 0;

	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);
	cq_free(items);

	return passes;
}

static cq_bool test_removeFirstItemIsTheSameOfRemoveItemAtZero()
{
	cq_core_List *list = cq_core_List_new();

	cq_suint **items = buildItems();

	cq_core_List_insertAsLastItem(list, items[0]);
	cq_core_List_insertAsLastItem(list, items[1]);
	cq_core_List_insertAsLastItem(list, items[2]);
	cq_core_List_insertAsLastItem(list, items[3]);

	cq_core_List_removeFirstItem(list);
	cq_bool passes = checkListItems(
		list,
		3,
		(cq_suint *[]) {items[1], items[2], items[3]}
	);

	if (passes)
	{
		cq_core_List_removeFirstItem(list);
		passes = checkListItems(
			list,
			2,
			(cq_suint *[]) {items[2], items[3]}
		);
	}

	if (passes)
	{
		cq_core_List_removeFirstItem(list);
		passes = checkListItems(
			list,
			1,
			(cq_suint *[]) {items[3]}
		);
	}

	if (passes)
	{
		cq_core_List_removeFirstItem(list);
		passes = cq_core_List_getSize(list) == 0;
	}

	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);
	cq_free(items[0]);
	cq_free(items[1]);
	cq_free(items[2]);
	cq_free(items[3]);
	cq_free(items);

	return passes;
}

static cq_bool test_removeLastItemIsTheSameOfRemoveItemAtSizeMinusOne()
{
	cq_core_List *list = cq_core_List_new();

	cq_suint **items = buildItems();

	cq_core_List_insertAsLastItem(list, items[0]);
	cq_core_List_insertAsLastItem(list, items[1]);
	cq_core_List_insertAsLastItem(list, items[2]);
	cq_core_List_insertAsLastItem(list, items[3]);

	cq_core_List_removeLastItem(list);
	cq_bool passes = checkListItems(
		list,
		3,
		(cq_suint *[]) {items[0], items[1], items[2]}
	);

	if (passes)
	{
		cq_core_List_removeLastItem(list);
		passes = checkListItems(
			list,
			2,
			(cq_suint *[]) {items[0], items[1]}
		);
	}

	if (passes)
	{
		cq_core_List_removeLastItem(list);
		passes = checkListItems(
			list,
			1,
			(cq_suint *[]) {items[0]}
		);
	}

	if (passes)
	{
		cq_core_List_removeLastItem(list);
		passes = cq_core_List_getSize(list) == 0;
	}

	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);
	cq_free(items[0]);
	cq_free(items[1]);
	cq_free(items[2]);
	cq_free(items[3]);
	cq_free(items);

	return passes;
}

static cq_bool test_isEmptyReturnsTrueIfSizeIsZero()
{
	cq_core_List *list = cq_core_List_new();

	cq_suint **items = buildItems();

	cq_bool passes = cq_core_List_isEmpty(list);

	if (passes)
	{
		cq_core_List_insertAsFirstItem(list, items[0]);
		passes = !cq_core_List_isEmpty(list);
	}

	if (passes)
	{
		cq_core_List_removeFirstItem(list);
		passes = cq_core_List_isEmpty(list);
	}

	cq_core_List_destroyListAndItems(list, &destroyItemWithoutLog);
	cq_free(items[0]);
	cq_free(items[1]);
	cq_free(items[2]);
	cq_free(items[3]);
	cq_free(items);

	return passes;
}



void cqTest_test_core_List_test()
{
// 	cqTest_Tester_runTest("test_newListWithInvalidChunkExits", &test_newListWithInvalidChunkExits);
	cqTest_Tester_runTest("test_newListHasSpecifiedReservedSizeAndChunkSize", &test_newListHasSpecifiedReservedSizeAndChunkSize);
	cqTest_Tester_runTest("test_newDefaultListHasDefaultReservedSizeAndChunkSize", &test_newDefaultListHasDefaultReservedSizeAndChunkSize);
	cqTest_Tester_runTest("test_newListHasSizeZero", &test_newListHasSizeZero);

// 	cqTest_Tester_runTest("test_insertItemAtInvalidIndexExits", &test_insertItemAtInvalidIndexExits);
	cqTest_Tester_runTest("test_insertItemIncreasesSize", &test_insertItemIncreasesSize);
	cqTest_Tester_runTest("test_destroyListAndItemsDestroysTheItems", &test_destroyListAndItemsDestroysTheItems);

// 	cqTest_Tester_runTest("test_getItemAtInvalidIndexExits", &test_getItemAtInvalidIndexExits);
	cqTest_Tester_runTest("test_getItemReturnsAPreviouslyInsertedItem", &test_getItemReturnsAPreviouslyInsertedItem);
	cqTest_Tester_runTest("test_getFirstItemReturnsTheItemAtIndexZero", &test_getFirstItemReturnsTheItemAtIndexZero);
	cqTest_Tester_runTest("test_getLastItemReturnsTheItemAtIndexSizeMinusOne", &test_getLastItemReturnsTheItemAtIndexSizeMinusOne);

// 	cqTest_Tester_runTest("test_setChunkAnInvalidChunkExits", &test_setChunkAnInvalidChunkExits);
	cqTest_Tester_runTest("test_insertingMoreItemsThanTheReservedSizeMakesTheStorageGrowByChunkSize", &test_insertingMoreItemsThanTheReservedSizeMakesTheStorageGrowByChunkSize);
	cqTest_Tester_runTest("test_insertItemsAtSomeIndexPushesBackPreviousItemsFromThatIndex", &test_insertItemsAtSomeIndexPushesBackPreviousItemsFromThatIndex);
	cqTest_Tester_runTest("test_insertAsFirstItemIsTheSameOfInsertItemAtZero", &test_insertAsFirstItemIsTheSameOfInsertItemAtZero);
	cqTest_Tester_runTest("test_insertAsLastItemIsTheSameOfInsertItemAtSize", &test_insertAsLastItemIsTheSameOfInsertItemAtSize);

// 	cqTest_Tester_runTest("test_removeItemAtInvalidIndexExits", &test_removeItemAtInvalidIndexExits);
	cqTest_Tester_runTest("test_removeItemDecreasesSize", &test_removeItemDecreasesSize);
	cqTest_Tester_runTest("test_removeItemMovesItemsForwardFromIndex", &test_removeItemMovesItemsForwardFromIndex);
	cqTest_Tester_runTest("test_removeItemShrinksStorage", &test_removeItemShrinksStorage);
	cqTest_Tester_runTest("test_removeItemCannotShrinkStorageToZero", &test_removeItemCannotShrinkStorageToZero);
	cqTest_Tester_runTest("test_removeFirstItemIsTheSameOfRemoveItemAtZero", &test_removeFirstItemIsTheSameOfRemoveItemAtZero);
	cqTest_Tester_runTest("test_removeLastItemIsTheSameOfRemoveItemAtSizeMinusOne", &test_removeLastItemIsTheSameOfRemoveItemAtSizeMinusOne);

	cqTest_Tester_runTest("test_isEmptyReturnsTrueIfSizeIsZero", &test_isEmptyReturnsTrueIfSizeIsZero);
}