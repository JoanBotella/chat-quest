#ifndef CQ_CORE_SLIDE_HEADER
#define CQ_CORE_SLIDE_HEADER

#include "chatQuest/core/global/global.h"
#include "chatQuest/core/slide/SlideTransition.h"

typedef struct _cq_core_Slide cq_core_Slide;

cq_core_Slide *cq_core_Slide_new(void);

void cq_core_Slide_destroy(cq_core_Slide *slide);

cq_bool cq_core_Slide_hasTitle(cq_core_Slide *slide);

cq_char *cq_core_Slide_getTitleAfterHas(cq_core_Slide *slide);

void cq_core_Slide_setTitle(cq_core_Slide *slide, const cq_char *title);

cq_bool cq_core_Slide_hasBody(cq_core_Slide *slide);

cq_char *cq_core_Slide_getBodyAfterHas(cq_core_Slide *slide);

void cq_core_Slide_setBody(cq_core_Slide *slide, const cq_char *body);

cq_core_SlideTransition cq_core_Slide_getTransition(cq_core_Slide *slide);

void cq_core_Slide_setTransition(cq_core_Slide *slide, cq_core_SlideTransition transition);

#endif