#include "chatQuest/core/global/global.h"
#include "chatQuest/core/slide/Slide.h"
#include "chatQuest/core/slide/SlideTransition.h"

struct _cq_core_Slide
{
	cq_char *title;
	cq_char *body;
	cq_core_SlideTransition transition;
};

cq_core_Slide *cq_core_Slide_new()
{
	cq_core_Slide *slide = cq_malloc(sizeof(cq_core_Slide));

	slide->title = NULL;
	slide->body = NULL;
	slide->transition = CQ_CORE_SLIDETRANSITION_NONE;

	return slide;
}

void cq_core_Slide_destroy(cq_core_Slide *slide)
{
	cq_free(slide->title);
	cq_free(slide->body);
	cq_free(slide);
}

cq_bool cq_core_Slide_hasTitle(cq_core_Slide *slide)
{
	return slide->title != NULL;
}

cq_char *cq_core_Slide_getTitleAfterHas(cq_core_Slide *slide)
{
	return slide->title;
}

void cq_core_Slide_setTitle(cq_core_Slide *slide, const cq_char *title)
{
	cq_free(slide->title);
	slide->title = cq_copyString(title);
}

cq_bool cq_core_Slide_hasBody(cq_core_Slide *slide)
{
	return slide->body != NULL;
}

cq_char *cq_core_Slide_getBodyAfterHas(cq_core_Slide *slide)
{
	return slide->body;
}

void cq_core_Slide_setBody(cq_core_Slide *slide, const cq_char *body)
{
	cq_free(slide->body);
	slide->body = cq_copyString(body);
}

cq_core_SlideTransition cq_core_Slide_getTransition(cq_core_Slide *slide)
{
	return slide->transition;
}

void cq_core_Slide_setTransition(cq_core_Slide *slide, cq_core_SlideTransition transition)
{
	slide->transition = transition;
}
