#include "chatQuest/core/global/global.h"
#include "chatQuest/core/list/List.h"

struct _cq_core_List
{
	void **storage;
	cq_uint size;
	cq_uint reservedSize;
	cq_uint chunkSize;
};

cq_core_List *cq_core_List_newByReservedSizeAndChunkSize(cq_uint reservedSize, cq_uint chunkSize)
{
	cq_core_List *list;

	if (chunkSize == 0)
	{
		cq_exitOnError(
			CQ_ERROR_CODE_LIST_INVALID_CHUNKSIZE,
			"cq_core_List invalid chunk size."
		);
	}

	list = cq_malloc(sizeof(cq_core_List));

	list->storage = cq_malloc(sizeof(void *) * reservedSize);
	list->size = 0;
	list->reservedSize = reservedSize;
	list->chunkSize = chunkSize;

	return list;
}

void cq_core_List_destroyListAndItems(cq_core_List *list, void (*destroyItem)(void *item))
{
	void **sNav = list->storage;
	void **limit = list->storage + list->size;

	while (sNav < limit)
	{
		destroyItem(*sNav);
		*++sNav;
	}

	cq_free(list->storage);
	cq_free(list);
}

cq_uint cq_core_List_getSize(cq_core_List *list)
{
	return list->size;
}

cq_uint cq_core_List_getReservedSize(cq_core_List *list)
{
	return list->reservedSize;
}

cq_uint cq_core_List_getChunkSize(cq_core_List *list)
{
	return list->chunkSize;
}

void cq_core_List_setChunkSize(cq_core_List *list, cq_uint chunkSize)
{
	list->chunkSize = chunkSize;
}

static void checkIndex(cq_uint index, cq_uint limit)
{
	if (index >= limit)
	{
		cq_exitOnError(
			CQ_ERROR_CODE_LIST_INDEX_OUT_OF_BOUNDS,
			"cq_core_List index out of bounds."
		);
	}
}

void *cq_core_List_getItemAt(cq_core_List *list, cq_uint index)
{
	checkIndex(index, list->size);
	return *(list->storage + index);
}

static void growStorage(cq_core_List *list)
{
	void **newStorage = cq_malloc(sizeof(void *) * (list->reservedSize + list->chunkSize));
	void **nsNav = newStorage;
	void **sNav = list->storage;
	void **limit = list->storage + list->size;

	while(sNav < limit)
	{
		*nsNav = *sNav;
		*++nsNav;
		*++sNav;
	}

	cq_free(list->storage);
	list->storage = newStorage;
	list->reservedSize += list->chunkSize;
}

void cq_core_List_insertItemAt(cq_core_List *list, void *item, cq_uint index)
{
	void **limit;
	void **sNav;

	checkIndex(index, list->size + 1);

	if (list->size == list->reservedSize)
	{
		growStorage(list);
	}

	limit = list->storage + index;
	sNav = list->storage + list->size;

	while(sNav > limit)
	{
		*sNav = *(sNav - 1);
		*--sNav;
	}

	*(list->storage + index) = item;
	++list->size;
}

static void shrinkStorage(cq_core_List *list)
{
	void **newStorage;
	void **nsNav;
	void **sNav;
	void **limit;

	if (list->reservedSize - list->chunkSize <= 0)
	{
		return;
	}

	newStorage = cq_malloc(sizeof(void *) * (list->reservedSize - list->chunkSize));
	nsNav = newStorage;
	sNav = list->storage;
	limit = list->storage + list->size;

	while(sNav < limit)
	{
		*nsNav = *sNav;
		*++nsNav;
		*++sNav;
	}

	cq_free(list->storage);
	list->storage = newStorage;
	list->reservedSize -= list->chunkSize;
}

void cq_core_List_removeItemAt(cq_core_List *list, cq_uint index)
{
	void **current;
	void **next;
	void **limit;

	checkIndex(index, list->size);

	current = list->storage + index;
	next = current + 1;
	limit = list->storage + list->size;

	while (next < limit)
	{
		*current = *next;
		*++current;
		*++next;
	}

	--list->size;

	if (list->size == list->reservedSize - list->chunkSize)
	{
		shrinkStorage(list);
	}
}
