#ifndef CQ_CORE_LIST_HEADER
#define CQ_CORE_LIST_HEADER

#include "chatQuest/core/global/global.h"

#define CQ_CORE_LIST_DEFAULT_RESERVEDSIZE 5
#define CQ_CORE_LIST_DEFAULT_CHUNKSIZE 5

typedef struct _cq_core_List cq_core_List;

cq_core_List *cq_core_List_newByReservedSizeAndChunkSize(cq_uint reservedSize, cq_uint chunkSize);

#define cq_core_List_new() cq_core_List_newByReservedSizeAndChunkSize(CQ_CORE_LIST_DEFAULT_RESERVEDSIZE, CQ_CORE_LIST_DEFAULT_CHUNKSIZE)

#define cq_core_List_destroyListButNotItems(list) cq_free(list)

void cq_core_List_destroyListAndItems(cq_core_List *list, void (*destroyItem)(void *item));

cq_uint cq_core_List_getSize(cq_core_List *list);

cq_uint cq_core_List_getReservedSize(cq_core_List *list);

cq_uint cq_core_List_getChunkSize(cq_core_List *list);

void cq_core_List_setChunkSize(cq_core_List *list, cq_uint chunkSize);

#define cq_core_List_isEmpty(list) cq_core_List_getSize(list) == 0

void *cq_core_List_getItemAt(cq_core_List *list, cq_uint index);

#define cq_core_List_getFirstItem(list) cq_core_List_getItemAt(list, 0)

#define cq_core_List_getLastItem(list) cq_core_List_getItemAt(list, cq_core_List_getSize(list) - 1)

void cq_core_List_insertItemAt(cq_core_List *list, void *item, cq_uint index);

#define cq_core_List_insertAsFirstItem(list, item) cq_core_List_insertItemAt(list, item, 0)

#define cq_core_List_insertAsLastItem(list, item) cq_core_List_insertItemAt(list, item, cq_core_List_getSize(list))

void cq_core_List_removeItemAt(cq_core_List *list, cq_uint index);

#define cq_core_List_removeFirstItem(list) cq_core_List_removeItemAt(list, 0)

#define cq_core_List_removeLastItem(list) cq_core_List_removeItemAt(list, cq_core_List_getSize(list) - 1)

#endif