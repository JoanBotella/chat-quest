#ifndef CQ_CORE_LOOPER_HEADER
#define CQ_CORE_LOOPER_HEADER

void cq_core_Looper_loop(
	void (*setupStatus)(void),
	void (*processRequestIntoResponse)(void)
);

#endif