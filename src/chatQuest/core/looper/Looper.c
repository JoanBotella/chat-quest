#include "chatQuest/core/looper/Looper.h"
#include "chatQuest/core/global/global.h"
#include "chatQuest/core/request/Request.h"
#include "chatQuest/core/response/Response.h"
#include "chatQuest/core/requestBuilder/RequestBuilder.h"
#include "chatQuest/core/responseDispatcher/ResponseDispatcher.h"
#include "chatQuest/core/status/Status.h"

void cq_core_Looper_loop(
	void (*setupStatus)(void),
	void (*processRequestIntoResponse)(void)
)
{
	cq_bool finish;

	setupStatus();

	cq_core_RequestBuilder_buildByStatus();

	do
	{
		cq_core_Response_setup();

		processRequestIntoResponse();

		cq_core_Request_tearDown();

		cq_core_ResponseDispatcher_dispatch();

		finish = cq_core_Response_getFinish();

		if (!finish)
		{
			cq_core_RequestBuilder_buildByResponse();
		}

		cq_core_Response_tearDown();
	}
	while (!finish);

	cq_core_Status_tearDown();
}
