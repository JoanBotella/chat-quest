#include "chatQuest/core/global/global.h"
#include "chatQuest/core/requestBuilder/RequestBuilder.h"
#include "chatQuest/core/request/Request.h"
#include "chatQuest/core/response/Response.h"
#include "chatQuest/core/prompter/Prompter.h"
#include "chatQuest/core/queryNormalizer/QueryNormalizer.h"

void cq_core_RequestBuilder_buildByStatus(void)
{
	cq_core_Request_setup();
}

static void cq_core_RequestBuilder_setupRequestQuery(void)
{
	cq_char *raw = cq_core_Prompter_prompt("> ");

	cq_char *normalized = cq_core_QueryNormalizer_normalize(raw);

	cq_free(raw);

	cq_core_Request_setQuery(normalized);

	cq_free(normalized);
}

void cq_core_RequestBuilder_buildByResponse(void)
{
	cq_core_RequestBuilder_buildByStatus();

	if (
		cq_core_List_isEmpty(
			cq_core_Response_getSlides()
		)
	)
	{
		return;
	}

	cq_core_RequestBuilder_setupRequestQuery();
}
