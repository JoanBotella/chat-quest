#ifndef CQ_CORE_REQUESTBUILDER_HEADER
#define CQ_CORE_REQUESTBUILDER_HEADER

void cq_core_RequestBuilder_buildByStatus(void);

void cq_core_RequestBuilder_buildByResponse(void);

#endif