#ifndef CQ_CORE_TAG_HEADER
#define CQ_CORE_TAG_HEADER

#if CQ_TARGET_BASH
	#include "chatQuest/target/bash/tag/tag.h"
#elif CQ_TARGET_C64
	#include "chatQuest/target/c64/tag/tag.h"
#else
	#error "Target not available. Check this file for enabled targets."
#endif

#endif