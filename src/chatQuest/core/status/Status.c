#include "chatQuest/core/global/global.h"
#include "chatQuest/core/status/Status.h"
#include "chatQuest/core/list/List.h"

struct _cq_core_Status
{
	cq_core_List *nodeIds;
	void (*destroyVariables)(void *);
};

static struct _cq_core_Status *status;

void cq_core_Status_setup(cq_core_List *nodeIds)
{
	status = cq_malloc(sizeof(struct _cq_core_Status));

	status->nodeIds = nodeIds;
	status->destroyVariables = NULL;
}

void cq_core_Status_destroyNodeIdsItem(void *nodeId)
{
	cq_free(nodeId);
}

void cq_core_Status_tearDown()
{
	cq_core_List_destroyListAndItems(status->nodeIds, &cq_core_Status_destroyNodeIdsItem);
	cq_free(status);
}

cq_core_List *cq_core_Status_getNodeIds()
{
	return status->nodeIds;
}
