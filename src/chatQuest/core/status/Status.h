#ifndef CQ_CORE_STATUS_HEADER
#define CQ_CORE_STATUS_HEADER

#include "chatQuest/core/list/List.h"

void cq_core_Status_setup(cq_core_List *nodeIds);

void cq_core_Status_tearDown(void);

void cq_core_Status_destroyNodeIdsItem(void *nodeId);

cq_core_List *cq_core_Status_getNodeIds(void);

#endif