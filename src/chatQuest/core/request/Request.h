#ifndef CQ_CORE_REQUEST_HEADER
#define CQ_CORE_REQUEST_HEADER

#include "chatQuest/core/global/global.h"

void cq_core_Request_setup(void);

void cq_core_Request_tearDown(void);

cq_bool cq_core_Request_hasQuery(void);

cq_char *cq_core_Request_getQueryAfterHas(void);

void cq_core_Request_setQuery(const cq_char *query);

#endif