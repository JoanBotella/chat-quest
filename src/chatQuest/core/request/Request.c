#include "chatQuest/core/global/global.h"
#include "chatQuest/core/request/Request.h"

struct _cq_core_Request
{
	cq_char *query;
};

static struct _cq_core_Request *request;

void cq_core_Request_setup()
{
	request = cq_malloc(sizeof(struct _cq_core_Request));

	request->query = NULL;
}

void cq_core_Request_tearDown(void)
{
	cq_free(request->query);
	cq_free(request);
}

cq_bool cq_core_Request_hasQuery(void)
{
	return request->query != NULL;
}

cq_char *cq_core_Request_getQueryAfterHas(void)
{
	return request->query;
}

void cq_core_Request_setQuery(const cq_char *query)
{
	cq_free(request->query);
	request->query = cq_copyString(query);
}