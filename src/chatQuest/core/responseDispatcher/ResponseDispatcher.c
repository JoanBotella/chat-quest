#include "chatQuest/core/responseDispatcher/ResponseDispatcher.h"
#include "chatQuest/core/global/global.h"
#include "chatQuest/core/list/List.h"
#include "chatQuest/core/response/Response.h"
#include "chatQuest/core/slide/Slide.h"
#include "chatQuest/core/slide/SlideTransition.h"
#include "chatQuest/core/prompter/Prompter.h"

#define CQ_CORE_RESPONSEDISPATCHER_DELAY_SECONDS 2

static void cq_core_ResponseDispatcher_dispatchSlideTitle(const cq_char *title)
{
	cq_formattedPrint("\n%s\n", title);
}

static void cq_core_ResponseDispatcher_dispatchSlideBody(const cq_char *body)
{
	cq_formattedPrint("\n%s\n\n", body);
}

static void cq_core_ResponseDispatcher_dispatchSlideTransitionPause(void)
{
	cq_free(
		cq_core_Prompter_prompt("[...]")
	);
}

static void cq_core_ResponseDispatcher_dispatchSlideTransitionDelay(void)
{
	cq_waitSeconds(CQ_CORE_RESPONSEDISPATCHER_DELAY_SECONDS);
}

static void cq_core_ResponseDispatcher_dispatchSlideTransition(cq_core_SlideTransition transition)
{
	switch (transition)
	{
		case CQ_CORE_SLIDETRANSITION_NONE:
			break;

		case CQ_CORE_SLIDETRANSITION_PAUSE:
			cq_core_ResponseDispatcher_dispatchSlideTransitionPause();
			break;

		case CQ_CORE_SLIDETRANSITION_DELAY:
			cq_core_ResponseDispatcher_dispatchSlideTransitionDelay();
			break;

		default:
			cq_exitOnError(
				CQ_ERROR_CODE_BAD_SLIDETRANSITION,
				"cq_core_ResponseDispatcher_dispatchSlideTransition could not dispatch transition."
			);
	}
}

static void cq_core_ResponseDispatcher_dispatchSlide(cq_core_Slide *slide)
{
	if (cq_core_Slide_hasTitle(slide))
	{
		cq_core_ResponseDispatcher_dispatchSlideTitle(
			cq_core_Slide_getTitleAfterHas(slide)
		);
	}

	if (cq_core_Slide_hasBody(slide))
	{
		cq_core_ResponseDispatcher_dispatchSlideBody(
			cq_core_Slide_getBodyAfterHas(slide)
		);
	}

	cq_core_ResponseDispatcher_dispatchSlideTransition(
		cq_core_Slide_getTransition(slide)
	);
}

void cq_core_ResponseDispatcher_dispatch()
{
	cq_int i = 0;
	cq_core_List *slides = cq_core_Response_getSlides();
	cq_int size = cq_core_List_getSize(slides);

	for (; i < size; i++)
	{
		cq_core_ResponseDispatcher_dispatchSlide(
			cq_core_List_getItemAt(slides, i)
		);
	}
}
