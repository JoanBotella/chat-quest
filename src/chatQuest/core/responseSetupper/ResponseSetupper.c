#include "chatQuest/core/responseSetupper/ResponseSetupper.h"
#include "chatQuest/core/list/List.h"
#include "chatQuest/core/slide/Slide.h"
#include "chatQuest/core/response/Response.h"
#include "chatQuest/core/status/Status.h"
#include "chatQuest/core/slide/SlideTransition.h"

void cq_core_ResponseSetupper_replaceNodeId(const cq_char *nodeId)
{
	cq_core_List *nodeIds = cq_core_Status_getNodeIds();
	cq_char *item = cq_core_List_getLastItem(nodeIds);

	cq_core_List_removeLastItem(nodeIds);
	cq_core_Status_destroyNodeIdsItem(item);

	cq_core_List_insertAsLastItem(
		nodeIds,
		cq_copyString(nodeId)
	);
}

void cq_core_ResponseSetupper_addNodeId(const cq_char *nodeId)
{
	cq_core_List_insertAsLastItem(
		cq_core_Status_getNodeIds(),
		cq_copyString(nodeId)
	);
}

void cq_core_ResponseSetupper_popNodeId()
{
	cq_core_List_removeLastItem(
		cq_core_Status_getNodeIds()
	);
}

void cq_core_ResponseSetupper_addSlide(cq_core_Slide *slide)
{
	cq_core_List_insertAsLastItem(
		cq_core_Response_getSlides(),
		slide
	);
}

void cq_core_ResponseSetupper_addBodySlide(const cq_char *body)
{
	cq_core_Slide *slide = cq_core_Slide_new();

	cq_core_Slide_setBody(slide, body);

	cq_core_ResponseSetupper_addSlide(slide);
}

void cq_core_ResponseSetupper_addTitleBodySlide(const cq_char *title, const cq_char *body)
{
	cq_core_Slide *slide = cq_core_Slide_new();

	cq_core_Slide_setTitle(slide, title);
	cq_core_Slide_setBody(slide, body);

	cq_core_ResponseSetupper_addSlide(slide);
}

void cq_core_ResponseSetupper_addTitleBodyTransitionSlide(const cq_char *title, const cq_char *body, cq_core_SlideTransition slideTransition)
{
	cq_core_Slide *slide = cq_core_Slide_new();

	cq_core_Slide_setTitle(slide, title);
	cq_core_Slide_setBody(slide, body);
	cq_core_Slide_setTransition(slide, slideTransition);

	cq_core_ResponseSetupper_addSlide(slide);
}
