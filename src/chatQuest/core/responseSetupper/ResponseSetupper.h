#ifndef CQ_CORE_RESPONSESETUPPER_HEADER
#define CQ_CORE_RESPONSESETUPPER_HEADER

#include "chatQuest/core/slide/Slide.h"
#include "chatQuest/core/slide/SlideTransition.h"

void cq_core_ResponseSetupper_replaceNodeId(const cq_char *nodeId);

void cq_core_ResponseSetupper_addNodeId(const cq_char *nodeId);

void cq_core_ResponseSetupper_popNodeId(void);

void cq_core_ResponseSetupper_addSlide(cq_core_Slide *slide);

void cq_core_ResponseSetupper_addBodySlide(const cq_char *body);

void cq_core_ResponseSetupper_addTitleBodySlide(const cq_char *title, const cq_char *body);

void cq_core_ResponseSetupper_addTitleBodyTransitionSlide(const cq_char *title, const cq_char *body, cq_core_SlideTransition slideTransition);

#endif