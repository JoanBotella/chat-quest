#ifndef CQ_CORE_RESPONSE_HEADER
#define CQ_CORE_RESPONSE_HEADER

#include "chatQuest/core/global/global.h"
#include "chatQuest/core/list/List.h"

void cq_core_Response_setup(void);

void cq_core_Response_tearDown(void);

cq_core_List *cq_core_Response_getSlides(void);

void cq_core_Response_setSlides(cq_core_List *slides);

cq_bool cq_core_Response_getFinish(void);

void cq_core_Response_setFinish(cq_bool finish);

#endif