#include "chatQuest/core/global/global.h"
#include "chatQuest/core/response/Response.h"
#include "chatQuest/core/slide/Slide.h"
#include "chatQuest/core/list/List.h"

struct _cq_core_Response
{
	cq_core_List *slides;
	cq_bool finish;
};

static struct _cq_core_Response *response;

void cq_core_Response_setup()
{
	response = cq_malloc(sizeof(struct _cq_core_Response));

	response->slides = cq_core_List_new();
	response->finish = CQ_FALSE;
}

void cq_core_Response_tearDownSlide(void *item)
{
	cq_core_Slide_destroy((cq_core_Slide *) item);
}

void cq_core_Response_tearDown(void)
{
	cq_core_List_destroyListAndItems(response->slides, &cq_core_Response_tearDownSlide);
	cq_free(response);
}

cq_core_List *cq_core_Response_getSlides(void)
{
	return response->slides;
}

void cq_core_Response_setSlides(cq_core_List *slides)
{
	cq_core_List_destroyListAndItems(response->slides, &cq_core_Response_tearDownSlide);
	response->slides = slides;
}

cq_bool cq_core_Response_getFinish(void)
{
	return response->finish;
}

void cq_core_Response_setFinish(cq_bool finish)
{
	response->finish = finish;
}
