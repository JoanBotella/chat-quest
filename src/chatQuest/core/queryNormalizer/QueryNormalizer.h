#ifndef CQ_CORE_QUERYNORMALIZER_HEADER
#define CQ_CORE_QUERYNORMALIZER_HEADER

#include "chatQuest/core/global/global.h"

cq_char *cq_core_QueryNormalizer_normalize(const cq_char *raw);

#endif