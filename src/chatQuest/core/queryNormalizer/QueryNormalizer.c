#include "chatQuest/core/queryNormalizer/QueryNormalizer.h"
#include "chatQuest/core/global/global.h"

cq_char *cq_core_QueryNormalizer_normalize(const cq_char *raw)
{
	cq_char *trimmed;
	cq_char *lowered = cq_stringToLowerCase(raw);

	trimmed = cq_trimString(lowered);

	cq_free(lowered);

	return trimmed;
}