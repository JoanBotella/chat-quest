
#define CQ_LANG_EN 1
#define CQ_LANG_ES 2

#if CQ_LANG == CQ_LANG_EN
	#include "chatQuest/core/translation/lang/en.h"
#elif CQ_LANG == CQ_LANG_ES
	#include "chatQuest/core/translation/lang/es.h"
#else
	#error "Translation not available. Check file for enabled translations."
#endif
