#include "chatQuest/core/global/global.h"
#include "chatQuest/core/node/Node.h"
#include "chatQuest/core/request/Request.h"
#include "chatQuest/core/response/Response.h"
#include "chatQuest/core/responseSetupper/ResponseSetupper.h"
#include "chatQuest/core/translation/translation.h"

void cq_core_Node_processRequestIntoResponse()
{
	cq_core_List *slides;
	cq_char *query = cq_core_Request_getQueryAfterHas();

	if (cq_areStringsEqual(query, CQ_T_NODE_QUERY_QUIT))
	{
		cq_core_ResponseSetupper_addBodySlide(CQ_T_NODE_QUIT);
		cq_core_Response_setFinish(CQ_TRUE);
		return;
	}

	slides = cq_core_Response_getSlides();

	cq_core_ResponseSetupper_addBodySlide(CQ_T_NODE_WHAT);
}
