#include "chatQuest/core/global/global.h"
#include "chatQuest/core/prompter/Prompter.h"

#define CQ_CORE_PROMPTER_BUFFER_SIZE 1000

cq_char *cq_core_Prompter_prompt(const cq_char *message)
{
	cq_print(message);
	return cq_readLineFromStdin(CQ_CORE_PROMPTER_BUFFER_SIZE);
}
