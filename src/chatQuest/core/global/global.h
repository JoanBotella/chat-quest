#ifndef CQ_CORE_GLOBAL_HEADER
#define CQ_CORE_GLOBAL_HEADER

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

typedef unsigned short int cq_suint;
typedef unsigned int cq_uint;
typedef unsigned long int cq_ulint;
typedef int cq_int;
typedef char cq_char;

typedef char cq_bool;
#define CQ_TRUE 1
#define CQ_FALSE 0

#define CQ_ERROR_CODE_MALLOC_COULD_NOT_ALLOCATE_MEMORY 1
#define CQ_ERROR_CODE_PROCESSOR_COULD_NOT_PROCESS 2
#define CQ_ERROR_CODE_NODE_COULD_NOT_PROCESS 3
#define CQ_ERROR_CODE_LIST_INDEX_OUT_OF_BOUNDS 4
#define CQ_ERROR_CODE_BAD_SLIDETRANSITION 5
#define CQ_ERROR_CODE_LIST_INVALID_CHUNKSIZE 6
#define CQ_ERROR_CODE_FORMATSTRING_COULD_NOT_ALLOCATE_MEMORY 7

#define cq_print(string) printf("%s", string)

#define cq_formattedPrint(template, ...) printf(template, __VA_ARGS__)

#define cq_exit(code) exit(code)

void cq_exitOnError(cq_uint code, const cq_char *template, ...);

void *cq_malloc(size_t size);

#define cq_free(pointer) free(pointer)

cq_char *cq_readLineFromStdin(cq_uint bufferSize);

#define cq_copyString(string) cq_copyStringFromIndexToSize(string, 0, cq_measureString(string))

cq_char *cq_copyStringFromIndexToSize(const cq_char *string, cq_uint index, cq_uint size);

#define cq_measureString(string) strlen(string)

cq_bool cq_areStringsEqual(const cq_char *a, const cq_char* b);

#define cq_waitSeconds(seconds) sleep(seconds)

cq_uint cq_countSubstrings(const cq_char *haystack, const cq_char *needle);

cq_char *cq_replaceSubstrings(const cq_char *haystack, const cq_char *needle, const cq_char *replacement);

cq_char *cq_concatStrings(cq_uint quantity, ...);

cq_char *cq_stringToLowerCase(const cq_char *string);

cq_char *cq_trimString(const cq_char *string);

cq_char *cq_formatString(const cq_char *template, ...);

#endif