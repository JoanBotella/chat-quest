#include "chatQuest/core/global/global.h"
#include <stdarg.h>
#include <ctype.h>
#include <stdio.h>

void cq_exitOnError(cq_uint code, const cq_char *template, ...)
{
	va_list args;
	va_start(args, template);
	vfprintf(stderr, template, args);
	va_end(args);

	fprintf(stderr, "\n");

	cq_exit(code);
}

void *cq_malloc(size_t size)
{
	void *pointer = malloc(size);

	if (pointer == NULL)
	{
		cq_exitOnError(
			CQ_ERROR_CODE_MALLOC_COULD_NOT_ALLOCATE_MEMORY,
			"cq_malloc could not allocate memory."
		);
	}

	return pointer;
}

cq_char *cq_readLineFromStdin(cq_uint bufferSize)
{
	cq_ulint lineSize;
	cq_char *line;
	cq_char *buffer = cq_malloc(bufferSize);
	cq_char *garbage = fgets(buffer, bufferSize, stdin);

	lineSize = cq_measureString(buffer) - 1;
	line = cq_copyStringFromIndexToSize(buffer, 0, lineSize);

	cq_free(buffer);

	return line;
}

cq_char *cq_copyStringFromIndexToSize(const cq_char *string, cq_uint index, cq_uint size)
{
	cq_char *r = cq_malloc(size + 1);
	cq_char *rNav = r;
	const cq_char *sNav = string;

	if (index > 0)
	{
		sNav += index;
		index = 0;
	}

	for (; size > 0 && *sNav != '\0'; --size)
	{
		*rNav = *sNav;

		++sNav;
		++rNav;
	}

	*rNav = '\0';

	return r;
}

cq_bool cq_areStringsEqual(const cq_char *a, const cq_char* b)
{
	const cq_char *aNav = a;
	const cq_char *bNav = b;

	while(CQ_TRUE)
	{
		if (*aNav != *bNav)
		{
			return CQ_FALSE;
		}

		if (*aNav == '\0')
		{
			return CQ_TRUE;
		}

		++aNav;
		++bNav;
	}
}

cq_uint cq_countSubstrings(const cq_char *haystack, const cq_char *needle)
{
	const cq_char *hNav = haystack;
	const cq_char *nNav = needle;

	cq_uint inNeedle = 0;
	cq_uint found = 0;

	for (; *hNav != '\0'; ++hNav)
	{
		if (*hNav == *nNav)
		{
			++inNeedle;

			++nNav;
			if (*nNav == '\0')
			{
				++found;
				nNav = needle;
				inNeedle = 0;
			}
		}
		else if (inNeedle)
		{
			hNav -= inNeedle;
			nNav = needle;
			inNeedle = 0;
		}
	}

	return found;
}

cq_char *cq_replaceSubstrings(const cq_char *haystack, const cq_char *needle, const cq_char *replacement)
{
	cq_ulint hSize = cq_measureString(haystack);

	cq_ulint nSize = cq_measureString(needle);
	cq_uint nCount = cq_countSubstrings(haystack, needle);

	cq_ulint rSize = cq_measureString(replacement);

	cq_char *changed = cq_malloc(hSize - (nSize * nCount) + (rSize * nCount) + 1);

	const cq_char *hNav = haystack;
	const cq_char *nNav = needle;
	const cq_char *rNav;
	cq_char *cNav = changed;

	cq_uint inNeedle = 0;

	for (; *hNav != '\0'; ++hNav)
	{
		if (*hNav == *nNav)
		{
			++inNeedle;

			++nNav;
			if (*nNav == '\0')
			{
				for (rNav = replacement; *rNav != '\0'; ++rNav)
				{
					*cNav = *rNav;
					++cNav;
				}

				nNav = needle;
				inNeedle = 0;
			}
		}
		else if (inNeedle)
		{
			hNav -= inNeedle;

			for (; inNeedle > 0; --inNeedle)
			{
				*cNav = *hNav;
				++cNav;
				++hNav;
			}
			--hNav;

			nNav = needle;
		}
		else
		{
			*cNav = *hNav;
			++cNav;
		}
	}

	*cNav = '\0';

	return changed;
}

cq_char *cq_concatStrings(cq_uint quantity, ...)
{
	va_list args1;
	va_list args2;
	cq_ulint size;
	cq_char *arg;
	cq_char *argNav;
	cq_char *concat;
	cq_char *concatNav;
	cq_uint i;

	va_start(args1, quantity);
	va_copy(args2, args1);

	size = 0;
	for (i = quantity; i > 0; --i)
	{
		arg = va_arg(args1, cq_char *);
		size += cq_measureString(arg);
	}
	va_end(args1);

	concat = cq_malloc(size + 1);
	concatNav = concat;

	for (i = quantity; i > 0; --i)
	{
		arg = va_arg(args2, cq_char *);
		for (argNav = arg; *argNav != '\0'; ++argNav)
		{
			*concatNav = *argNav;
			++concatNav;
		}
	}
	va_end(args2);

	*concatNav = '\0';

	return concat;
}

cq_char *cq_stringToLowerCase(const cq_char *string)
{
	cq_char *r = malloc(sizeof(cq_char *) * cq_measureString(string));

	const cq_char *sNav = string;
	cq_char *rNav = r;
	while (*sNav != '\0')
	{
		*rNav = tolower(*sNav);
		++sNav;
		++rNav;
	}

	*rNav = '\0';

	return r;
}

cq_char *cq_trimString(const cq_char *string)
{
	cq_ulint leftSpaces = 0;
	cq_ulint size;

	const cq_char *sNav = string;
	while (
		*sNav == ' '
		|| *sNav == '\t'
	)
	{
		++leftSpaces;
		++sNav;
	}

	size = 0;

	while (*sNav != '\0')
	{
		++size;
		++sNav;
	}
	--sNav;

	while (
		size > 0
		&& (
			*sNav == ' '
			|| *sNav == '\t'
		)
	)
	{
		--size;
		--sNav;
	}

	return cq_copyStringFromIndexToSize(string, leftSpaces, size);
}

cq_char *cq_formatString(const cq_char *template, ...)
{
	va_list args1;
	va_list args2;
	cq_ulint size;
	cq_char *string;

	va_start(args1, template);
	va_copy(args2, args1);

	size = vsnprintf(NULL, 0, template, args1) + 1;
	va_end(args1);

	string = cq_malloc(size);
	vsnprintf(string, size, template, args2);
	va_end(args2);

	return string;
}
