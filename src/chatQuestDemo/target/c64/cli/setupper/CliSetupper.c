#include "chatQuestDemo/target/c64/cli/setupper/CliSetupper.h"
#include "chatQuestDemo/target/c64/cli/cliColor.h"
#include <conio.h>

void cqDemo_target_c64_CliSetupper_setup(void)
{
	bgcolor(CQ_TARGET_C64_CLICOLOR_BLACK);
	bordercolor(CQ_TARGET_C64_CLICOLOR_BLACK);
	textcolor(CQ_TARGET_C64_CLICOLOR_GRAY1);
}