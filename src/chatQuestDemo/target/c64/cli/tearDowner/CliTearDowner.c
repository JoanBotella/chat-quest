#include "chatQuestDemo/target/c64/cli/tearDowner/CliTearDowner.h"
#include "chatQuestDemo/target/c64/cli/cliColor.h"
#include <conio.h>

void cqDemo_target_c64_CliTearDowner_tearDown(void)
{
	bgcolor(CQ_TARGET_C64_CLICOLOR_BLUE);
	bordercolor(CQ_TARGET_C64_CLICOLOR_LIGHTBLUE);
	textcolor(CQ_TARGET_C64_CLICOLOR_LIGHTBLUE);
}