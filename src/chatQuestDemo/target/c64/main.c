#include "chatQuest/core/global/global.h"
#include "chatQuestDemo/core/app/App.h"
#include "chatQuestDemo/target/c64/cli/setupper/CliSetupper.h"
#include "chatQuestDemo/target/c64/cli/tearDowner/CliTearDowner.h"

cq_int main(void)
{
	cqDemo_core_App_runWithCliSetupAndTearDown(
		&cqDemo_target_c64_CliSetupper_setup,
		&cqDemo_target_c64_CliTearDowner_tearDown
	);
	return 0;
}