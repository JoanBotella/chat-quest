#include "chatQuest/core/global/global.h"
#include "chatQuestDemo/core/app/App.h"

#if CQ_ENV_DEV

#include "chatQuestTest/tester/Tester.h"

cq_int main(cq_int argc, cq_char **argv)
{
	if (
		argc > 1
		&& cq_areStringsEqual("test", argv[1])
	)
	{
		return cqTest_Tester_test();
	}
	cqDemo_core_App_run();
	return 0;
}

#elif CQ_ENV_PROD

cq_int main(cq_int argc, cq_char **argv)
{
	cqDemo_core_App_run();
	return 0;
}

#endif
