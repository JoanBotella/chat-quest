#ifndef CQDEMO_CORE_VARIABLES_HEADER
#define CQDEMO_CORE_VARIABLES_HEADER

#include "chatQuest/core/global/global.h"

void cqDemo_core_Variables_setup(void);

void cqDemo_core_Variables_tearDown(void);

void cqDemo_core_Variables_setGreetedTimes(cq_uint greetedTimes);

cq_uint cqDemo_core_Variables_getGreetedTimes(void);

#endif