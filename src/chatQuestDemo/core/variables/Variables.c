#include "chatQuest/core/global/global.h"
#include "chatQuestDemo/core/variables/Variables.h"

struct _cqDemo_core_Variables
{
	cq_uint greetedTimes;
};

static struct _cqDemo_core_Variables *variables;

void cqDemo_core_Variables_setup(void)
{
	variables = cq_malloc(sizeof(struct _cqDemo_core_Variables));

	variables->greetedTimes = 0;
}

void cqDemo_core_Variables_tearDown(void)
{
	cq_free(variables);
}

void cqDemo_core_Variables_setGreetedTimes(cq_uint greetedTimes)
{
	variables->greetedTimes = greetedTimes;
}

cq_uint cqDemo_core_Variables_getGreetedTimes(void)
{
	return variables->greetedTimes;
}
