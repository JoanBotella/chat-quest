#include "chatQuest/core/global/global.h"
#include "chatQuestDemo/core/processor/Processor.h"
#include "chatQuest/core/request/Request.h"
#include "chatQuest/core/list/List.h"
#include "chatQuest/core/status/Status.h"

#include "chatQuestDemo/core/node/bedroom/Bedroom.h"
#include "chatQuestDemo/core/node/corridor/Corridor.h"

void cqDemo_core_Processor_processRequestIntoResponse()
{
	cq_char *nodeId = cq_core_List_getLastItem(
		cq_core_Status_getNodeIds()
	);

	if (cq_areStringsEqual(nodeId, CQDEMO_CORE_NODE_BEDROOM_ID)) { cqDemo_core_node_Bedroom_processRequestIntoResponse(); }
	else if (cq_areStringsEqual(nodeId, CQDEMO_CORE_NODE_CORRIDOR_ID)) { cqDemo_core_node_Corridor_processRequestIntoResponse(); }
	else
	{
		cq_exitOnError(
			CQ_ERROR_CODE_PROCESSOR_COULD_NOT_PROCESS,
			"cqDemo_core_Processor_processRequestIntoResponse could not processRequestIntoResponse request."
		);
	}
}
