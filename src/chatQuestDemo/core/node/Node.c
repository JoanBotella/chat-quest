#include "chatQuest/core/global/global.h"
#include "chatQuestDemo/core/node/Node.h"
#include "chatQuest/core/request/Request.h"
#include "chatQuest/core/responseSetupper/ResponseSetupper.h"
#include "chatQuest/core/node/Node.h"
#include "chatQuestDemo/core/translation/translation.h"

void cqDemo_core_Node_processRequestIntoResponse()
{
	cq_char *query = cq_core_Request_getQueryAfterHas();

	if (cq_areStringsEqual(query, CQDEMO_T_NODE_QUERY_HELP))
	{
		cq_core_ResponseSetupper_addBodySlide(
			CQDEMO_T_NODE_HELP
		);
		return;
	}

	cq_core_Node_processRequestIntoResponse();
}
