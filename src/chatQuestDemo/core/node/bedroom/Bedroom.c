#include "chatQuest/core/global/global.h"
#include "chatQuestDemo/core/node/bedroom/Bedroom.h"
#include "chatQuestDemo/core/node/corridor/Corridor.h"
#include "chatQuest/core/request/Request.h"
#include "chatQuest/core/responseSetupper/ResponseSetupper.h"
#include "chatQuestDemo/core/node/Node.h"
#include "chatQuest/core/response/Response.h"
#include "chatQuestDemo/core/variables/Variables.h"
#include "chatQuestDemo/core/translation/translation.h"

void cqDemo_core_node_Bedroom_processRequestIntoResponse()
{
	cq_char *query;
	cq_char *body;
	cq_uint greetedTimes;

	if (!cq_core_Request_hasQuery())
	{
		cq_core_ResponseSetupper_addTitleBodySlide(
			CQDEMO_T_BEDROOM_TITLE,
			CQDEMO_T_BEDROOM_LOOK
		);
		return;
	}

	query = cq_core_Request_getQueryAfterHas();

	if (cq_areStringsEqual(query, CQDEMO_T_BEDROOM_QUERY_GREET_WORLD))
	{
		greetedTimes = cqDemo_core_Variables_getGreetedTimes();

		body = cq_formatString(CQDEMO_T_BEDROOM_GREET_WORLD, greetedTimes);
		cq_core_ResponseSetupper_addBodySlide(body);
		cq_free(body);

		cqDemo_core_Variables_setGreetedTimes(greetedTimes + 1);

		return;
	}

	if (cq_areStringsEqual(query, CQDEMO_T_BEDROOM_QUERY_GO_CORRIDOR))
	{
		cq_core_ResponseSetupper_replaceNodeId(CQDEMO_CORE_NODE_CORRIDOR_ID);
		return;
	}

	cqDemo_core_Node_processRequestIntoResponse();
}
