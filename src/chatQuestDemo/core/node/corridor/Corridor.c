#include "chatQuest/core/global/global.h"
#include "chatQuestDemo/core/node/corridor/Corridor.h"
#include "chatQuestDemo/core/node/bedroom/Bedroom.h"
#include "chatQuest/core/request/Request.h"
#include "chatQuest/core/responseSetupper/ResponseSetupper.h"
#include "chatQuest/core/slide/SlideTransition.h"
#include "chatQuestDemo/core/node/Node.h"
#include "chatQuestDemo/core/translation/translation.h"

void cqDemo_core_node_Corridor_processRequestIntoResponse()
{
	cq_char *query;

	if (!cq_core_Request_hasQuery())
	{
		cq_core_ResponseSetupper_addTitleBodyTransitionSlide(
			CQDEMO_T_CORRIDOR_TITLE,
			CQDEMO_T_CORRIDOR_LOOK,
			CQ_CORE_SLIDETRANSITION_PAUSE
		);
		cq_core_ResponseSetupper_addBodySlide(
			CQDEMO_T_CORRIDOR_HEAR
		);
		return;
	}

	query = cq_core_Request_getQueryAfterHas();

	if (cq_areStringsEqual(query, CQDEMO_T_CORRIDOR_QUERY_GO_BEDROOM))
	{
		cq_core_ResponseSetupper_replaceNodeId(CQDEMO_CORE_NODE_BEDROOM_ID);
		return;
	}

	cqDemo_core_Node_processRequestIntoResponse();
}
