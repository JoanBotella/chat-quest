#include "chatQuest/core/tag/tag.h"

#define CQDEMO_T_NODE_QUERY_HELP "/ayuda"
#define CQDEMO_T_NODE_HELP "Verbos disponibles: saludar, ir\n\nComandos disponibles: /ayuda, /salir"

#define CQDEMO_T_BEDROOM_TITLE CQ_CORE_TAG_TITLE "Dormitorio" CQ_CORE_TAG_RESET
#define CQDEMO_T_BEDROOM_LOOK "Estás en tu dormitorio.\n\nTu " CQ_CORE_TAG_ITEM "cama" CQ_CORE_TAG_RESET " está aquí.\n\nPuedes ir al " CQ_CORE_TAG_EXIT "pasillo" CQ_CORE_TAG_RESET "."
#define CQDEMO_T_BEDROOM_QUERY_GREET_WORLD "saludar mundo"
#define CQDEMO_T_BEDROOM_GREET_WORLD "¡Hola Mundo! Ya has dicho esto %d veces."
#define CQDEMO_T_BEDROOM_QUERY_GO_CORRIDOR "ir pasillo"

#define CQDEMO_T_CORRIDOR_TITLE CQ_CORE_TAG_TITLE "Pasillo" CQ_CORE_TAG_RESET
#define CQDEMO_T_CORRIDOR_LOOK "Estás en el pasillo.\n\nTu perrita " CQ_CORE_TAG_CHARACTER "Zelda" CQ_CORE_TAG_RESET " está aquí, mirándote. Tal vez quieras " CQ_CORE_TAG_VERB "hablar" CQ_CORE_TAG_RESET " con ella.\n\nPuedes ir al " CQ_CORE_TAG_EXIT "dormitorio" CQ_CORE_TAG_RESET "."
#define CQDEMO_T_CORRIDOR_HEAR "Oyes algo que viene de la cocina."
#define CQDEMO_T_CORRIDOR_QUERY_GO_BEDROOM "ir dormitorio"
