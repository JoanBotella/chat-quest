#include "chatQuest/core/tag/tag.h"

#define CQDEMO_T_NODE_QUERY_HELP "/help"
#define CQDEMO_T_NODE_HELP "Available verbs: greet, go\n\nAvailable commands: /help, /quit"

#define CQDEMO_T_BEDROOM_TITLE CQ_CORE_TAG_TITLE "Bedroom" CQ_CORE_TAG_RESET
#define CQDEMO_T_BEDROOM_LOOK "You are in your bedroom.\n\nHere is your " CQ_CORE_TAG_ITEM "bed" CQ_CORE_TAG_RESET ".\n\nYou can go to the " CQ_CORE_TAG_EXIT "corridor" CQ_CORE_TAG_RESET "."
#define CQDEMO_T_BEDROOM_QUERY_GREET_WORLD "greet world"
#define CQDEMO_T_BEDROOM_GREET_WORLD "Hello World! You said this %d time(s) before."
#define CQDEMO_T_BEDROOM_QUERY_GO_CORRIDOR "go corridor"

#define CQDEMO_T_CORRIDOR_TITLE CQ_CORE_TAG_TITLE "Corridor" CQ_CORE_TAG_RESET
#define CQDEMO_T_CORRIDOR_LOOK "You are in the corridor.\n\nYour doggy " CQ_CORE_TAG_CHARACTER "Zelda" CQ_CORE_TAG_RESET " is here, looking at you. You may want to " CQ_CORE_TAG_VERB "talk" CQ_CORE_TAG_RESET " to her.\n\nYou can go to the " CQ_CORE_TAG_EXIT "bedroom" CQ_CORE_TAG_RESET "."
#define CQDEMO_T_CORRIDOR_HEAR "You can hear something coming from the kitchen."
#define CQDEMO_T_CORRIDOR_QUERY_GO_BEDROOM "go bedroom"
