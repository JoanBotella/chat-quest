
#define CQDEMO_LANG_EN 1
#define CQDEMO_LANG_ES 2

#if CQDEMO_LANG == CQDEMO_LANG_EN
	#include "chatQuestDemo/core/translation/lang/en.h"
#elif CQDEMO_LANG == CQDEMO_LANG_ES
	#include "chatQuestDemo/core/translation/lang/es.h"
#else
	#error "Translation not available. Check this file for enabled translations."
#endif
