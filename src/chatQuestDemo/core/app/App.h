#ifndef CQDEMO_CORE_APP_HEADER
#define CQDEMO_CORE_APP_HEADER

void cqDemo_core_App_run(void);

void cqDemo_core_App_runWithCliSetupAndTearDown(
	void (*setupCli)(void),
	void (*tearDownCli)(void)
);

#endif