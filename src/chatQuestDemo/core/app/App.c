#include "chatQuestDemo/core/app/App.h"
#include "chatQuest/core/looper/Looper.h"
#include "chatQuest/core/responseDispatcher/ResponseDispatcher.h"
#include "chatQuestDemo/core/statusSetupper/StatusSetupper.h"
#include "chatQuestDemo/core/processor/Processor.h"
#include "chatQuestDemo/core/variables/Variables.h"

void cqDemo_core_App_run(void)
{
	cqDemo_core_Variables_setup();

	cq_core_Looper_loop(
		&cqDemo_core_StatusSetupper_setupStatus,
		&cqDemo_core_Processor_processRequestIntoResponse
	);

	cqDemo_core_Variables_tearDown();
}

void cqDemo_core_App_runWithCliSetupAndTearDown(
	void (*setupCli)(void),
	void (*tearDownCli)(void)
)
{
	setupCli();
	cqDemo_core_App_run();
	tearDownCli();
}
