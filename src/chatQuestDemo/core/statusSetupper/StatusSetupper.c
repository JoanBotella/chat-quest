#include "chatQuestDemo/core/statusSetupper/StatusSetupper.h"
#include "chatQuest/core/list/List.h"
#include "chatQuest/core/status/Status.h"
#include "chatQuestDemo/core/node/bedroom/Bedroom.h"

void cqDemo_core_StatusSetupper_setupStatus(void)
{
	cq_core_List *nodeIds = cq_core_List_new();
	cq_core_List_insertAsLastItem(nodeIds, cq_copyString(CQDEMO_CORE_NODE_BEDROOM_ID));

	cq_core_Status_setup(nodeIds);
}